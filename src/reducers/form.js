import {SEND_FORM_DONE} from '../actions/form';

const defaultState = {
    formSend: false
};

function formReducer(state = defaultState, action) {

    switch(action.type) {
        case SEND_FORM_DONE:
            return {
                ...state,
                formSend: true
            };

        default:
            return state;
    }

}

export default formReducer;