import {rootEpic} from './epics';
import reducers from './reducers';

import {createStore, applyMiddleware, compose} from 'redux';
import {createEpicMiddleware} from 'redux-observable';

const epicMiddleware = createEpicMiddleware();

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        reducers,
        composeEnhancer(
            applyMiddleware(epicMiddleware),
        )
    );

    epicMiddleware.run(rootEpic);

    return {
        store,
    };
}