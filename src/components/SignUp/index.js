import React, { useState } from 'react';
import {connect} from 'react-redux';

import {sendSignUp} from '../../actions/form';

function SignUp({sendSignUp}) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        sendSignUp({email, password});
    };

    return (
        <form action={process.env.REACT_APP_AUTH_API + process.env.REACT_APP_AUTH_API_SIGN_UP} method="post" onSubmit={handleSubmit}>
            <h3>SignUp:</h3>

            <label>
                Email:
                <input
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    placeholder="Email.."
                    type="text"
                    name="email"
                    required
                    autoComplete="email"
                    autoFocus
                />
            </label>
            <br />
            <label>
                Password:
                <input
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    placeholder="Password.."
                    type="password"
                    name="password"
                    required
                    autoComplete="password"
                />
            </label>

            <input type="submit" value="Submit" />
        </form>
    );
}

export default connect(
    null,
    {sendSignUp}
)(SignUp);
