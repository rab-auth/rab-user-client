import React, {useState} from 'react';
import {connect} from 'react-redux';

import {getProfile, sendLogin} from '../../actions/form';

function Login({sendLogin, getProfile}) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        sendLogin({email, password});
    };

    return (
        <div>
            <form action={process.env.REACT_APP_AUTH_API + process.env.REACT_APP_AUTH_API_LOGIN} method="post"
                  onSubmit={handleSubmit}>
                <h3>Login:</h3>

                <label>
                    Email:
                    <input
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        placeholder="Email.."
                        type="text"
                        name="email"
                        required
                        autoComplete="email"
                        autoFocus
                    />
                </label>
                <br/>
                <label>
                    Password:
                    <input
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        placeholder="Password.."
                        type="password"
                        name="password"
                        required
                        autoComplete="password"
                    />
                </label>

                <input type="submit" value="Submit"/>
            </form>

            <button onClick={getProfile}>GetProfile</button>
        </div>
    );
}

export default connect(
    null,
    {sendLogin, getProfile}
)(Login);
