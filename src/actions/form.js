export const SEND_FORM = 'SEND_FORM';
export const SEND_FORM_DONE = 'SEND_FORM_DONE';
export const GET_PROFILE = 'GET_PROFILE';

export const sendForm = (endpoint) => (form) => ({
    type: SEND_FORM,
    payload: {
        endpoint,
        form
    }
});

export const sendLogin = sendForm(process.env.REACT_APP_AUTH_API_LOGIN);
export const sendSignUp = sendForm(process.env.REACT_APP_AUTH_API_SIGN_UP);

export const sendFormDone = (payload) => ({
    type: SEND_FORM_DONE,
    payload,
});

export const getProfile = () => ({
    type: GET_PROFILE
});