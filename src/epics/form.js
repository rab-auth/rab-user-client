import {ofType} from 'redux-observable';
import {map, mergeMap, catchError, tap, ignoreElements} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
import Cookies from 'js-cookie';

import {
    SEND_FORM,
    GET_PROFILE,
    sendFormDone
} from '../actions/form';

/**
 * Send the form and give confirmation
 * @param action$
 * @param state
 */
export const sendFormEpic = (action$, state) => action$.pipe(
    ofType(SEND_FORM),
    mergeMap(({payload: {endpoint, form}}) => ajax.post(endpoint, form)
        .pipe(
            tap(({response}) => console.log('sendFormEpic', response))
        )
    ),
    ignoreElements(),
    catchError(error => {
        console.error(error)
    })
);

/**
 * Send the form and give confirmation
 * @param action$
 * @param state
 */
export const getProfile = (action$, state) => action$.pipe(
    ofType(GET_PROFILE),
    mergeMap(action => ajax.get(process.env.REACT_APP_AUTH_API_PROFILE, {
            Authorization: `Bearer ${Cookies.get('user_token')}`,
        })
        .pipe(
            tap(({response}) => console.log('getProfile', response))
        )
    ),
    ignoreElements(),
    catchError(error => {
        console.error(error)
    })
);
