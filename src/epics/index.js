import {combineEpics} from 'redux-observable';
import {getProfile, sendFormEpic} from "./form";

export const rootEpic = combineEpics(
    sendFormEpic,
    getProfile
);